// import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router, Switch, Route, } from 'react-router-dom'
import FormCreateComp from './components/FormComp/FormCreateComp'
import FormEditComp from './components/FormComp/FormEditComp'
import HomeComp from './components/HomeComp/HomeComp'
import PageNotFound from './components/PageNotFound';
import NavbarPage from './components/NavbarPage/NavbarPage';
import ResultComp from './components/ResultComp'
function App() {
  return (
    <Router>
      <NavbarPage/>
      <Switch>
        <Route path="/" exact component= {HomeComp} />
        <Route path="/register" exact component= {FormCreateComp} />
        <Route path="/edit" exact component={FormEditComp} />
        <Route path="/result/:username/:email" exact component= {ResultComp} />
        <Route path="*" exact component={PageNotFound} />
      </Switch>
    </Router>
  );
}

export default App;
