import React from 'react'
import {Link, useLocation} from 'react-router-dom'

function PageNotFound() {
    let location = useLocation()
    return (
        <div className="d-flex justify-content-center my-lg-5">
            <h2>Sorry, the page {location.pathname} doesn't exist! <Link to="/">Go Home</Link> </h2>
        </div>
    )
}

export default PageNotFound
