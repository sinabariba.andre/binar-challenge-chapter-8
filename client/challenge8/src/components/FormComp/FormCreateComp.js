import React,{Component} from 'react'
import {Form, Button} from 'react-bootstrap'
import axios from 'axios'



class FormCreateComp extends Component{
    constructor(props){
        super(props)

        this.state ={
            username:'',
            email:'',
            password:'',
            experience:'',
            lvl:'0'
        }
    }

        changeHandler = (event)=>{
            this.setState({ [event.target.name]: event.target.value})
        }
        submitHandler = event =>{
            event.preventDefault()
            axios.post('https://jsonplaceholder.typicode.com/posts',this.state)
            .then(response =>{
                this.props.history.push(`/result/${response.data.username}/${response.data.email}`)
             console.log(response)
            })
            .catch(error =>{
                console.log(error)
            })
        }

render(){    
    const { username, email, password, experience}= this.state
    return (
        <div className="d-flex justify-content-center  m-5">
           <Form className="col-md-7" onSubmit={this.submitHandler} method="post" action={`/result`}>
            <h2 className="App">Create Account</h2>
                <Form.Group >
                    <Form.Label>Username</Form.Label>
                    <Form.Control  type="text" value={username} onChange={this.changeHandler} name="username" id="username" placeholder="Example therock"></Form.Control>
                </Form.Group> 
                <Form.Group >
                    <Form.Label>Email</Form.Label>
                    <Form.Control  type="email" value={email} onChange={this.changeHandler} name="email" id="email" placeholder="Example therock@gmail.com"></Form.Control>
                </Form.Group> 
                <Form.Group >
                    <Form.Label>Password</Form.Label>
                    <Form.Control  type="password" value={password} onChange={this.changeHandler} name="password" id="password" placeholder="*****"></Form.Control>
                </Form.Group> 
                <Form.Group >
                    <Form.Label>Experience</Form.Label>
                    <Form.Control  type="text" value={experience} onChange={this.changeHandler} name="experience" id="experience" placeholder="0"></Form.Control>
                </Form.Group> 
                <Form.Group>
                    {/* <Form.Label>Level</Form.Label> */}
                    <Form.Control  type="hidden" value="0" onChange={this.changeHandler} name="lvl" id="lvl" placeholder="0"></Form.Control>
                </Form.Group> 
                <Button type="submit">Submit</Button>
           </Form>
        </div>
    )
    }
}

export default FormCreateComp