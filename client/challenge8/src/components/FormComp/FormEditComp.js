import React from 'react'
import { Form ,Button} from 'react-bootstrap'

function FormEditComp(){
    return(
        <div className="d-flex justify-content-center  m-5">
        <Form className="col-md-7" method="post" action="#">
         <h2 className="App">Update Account</h2>
             <Form.Group >
                 <Form.Label>Username</Form.Label>
                 <Form.Control  type="text" name="username" id="username" placeholder="Example therock"></Form.Control>
             </Form.Group> 
             <Form.Group >
                 <Form.Label>Email</Form.Label>
                 <Form.Control  type="email" name="email" id="email" placeholder="Example therock@gmail.com"></Form.Control>
             </Form.Group>
             <Button type="submit">Update</Button>
        </Form>
        </div>
    )

}

export default FormEditComp