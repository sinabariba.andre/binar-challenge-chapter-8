import React from 'react'
import {useParams} from 'react-router-dom'

function ResultComp(props) {
    let {username, email} = useParams()
    
    return (
        <div className="container-fluid m-5 ">
            
            <h2> Username: {username}</h2>
            <h2>Email: {email}</h2>
            
          
        </div>
    )
}

export default ResultComp
